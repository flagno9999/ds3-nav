package com.example.app_nav.ui.calculadora;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.app_nav.R;

public class CalculadoraFragment extends Fragment {

    EditText et1;
    EditText et2;
    EditText etResult;
    Button sumar,restar,multiplicar,dividir,factorail,seno,tangente,potencia,limpiar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_calculadora, container, false);
       //final TextView textView = root.findViewById(R.id.text_home);
        Toast.makeText(getContext(),"Calculadora inflated",Toast.LENGTH_LONG).show();
        et1 = root.findViewById(R.id.et1);
        et2 = root.findViewById(R.id.et2);
        etResult =  root.findViewById(R.id.etResult);


        sumar =  root.findViewById(R.id.btsumar);
        sumar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               sumar();
            }
        });

        restar =  root.findViewById(R.id.btrestar);
        restar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                restar();
            }
        });

        multiplicar =  root.findViewById(R.id.btcalcular);
        multiplicar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               calcular();
            }
        });

        dividir =  root.findViewById(R.id.btdivision);
        dividir.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              dividir();
            }
        });

        potencia =  root.findViewById(R.id.btPotencia);
        potencia.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               potencia();
            }
        });

        factorail =  root.findViewById(R.id.btFactorial);
        factorail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                factorial();
            }
        });

        seno =  root.findViewById(R.id.btseno);
        seno.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                seno();
            }
        });

        tangente =  root.findViewById(R.id.bttangente);
        tangente.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               tangente();
            }
        });

        limpiar =  root.findViewById(R.id.btlimpiar);
        limpiar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                limpiar();
            }
        });

        return root;
    }

    public void sumar(){
        if(validarDigitos()){
            etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) + Double.parseDouble(et2.getText().toString() ))  );
        }else{
            Toast.makeText(getContext(),"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }

    }

    public void restar(){

        if(validarDigitos()){
            etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) - Double.parseDouble(et2.getText().toString() ))  );
        }else{
            Toast.makeText(getContext(),"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void calcular(){

        if(validarDigitos()){
            etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) * Double.parseDouble(et2.getText().toString() ))  );
        }else{
            Toast.makeText(getContext(),"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void dividir(){
        if(et2.getText().equals("0")){
            Toast.makeText(getContext(),"El segundo numero no puede ser 0",Toast.LENGTH_LONG).show();
        }else{
            if(validarDigitos()){
                etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) / Double.parseDouble(et2.getText().toString() ))  );
            }else{
                Toast.makeText(getContext(),"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
            }
        }

    }

    public void potencia(){

        if(validarDigitos()){
            etResult.setText( ""+ (Math.pow(Double.parseDouble(et1.getText().toString() ) , Double.parseDouble(et2.getText().toString() )))  );
        }else{
            Toast.makeText(getContext(),"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void factorial(){
        etResult.setText( ""+ (calculoFactorial(Double.parseDouble(et1.getText().toString() )))  );

    }

    public void seno(){
        etResult.setText( ""+ (Math.sin(Double.parseDouble(et1.getText().toString() )))  );
    }

    public void tangente(){
        etResult.setText( ""+ (Math.tan(Double.parseDouble(et1.getText().toString() )))  );
    }

    public void limpiar(){
        et1.setText("");
        et2.setText("");
        etResult.setText("");
    }

    public double calculoFactorial (double numero) {
        if (numero==0)
            return 1;
        else
            return numero * calculoFactorial(numero-1);
    }

    public boolean validarDigitos(){
        if(et1.getText().length()>0 && et2.getText().length()>0){
            return true;
        }else{
            return false;
        }
    }
}