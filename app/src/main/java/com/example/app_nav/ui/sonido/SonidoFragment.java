package com.example.app_nav.ui.sonido;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.app_nav.R;

public class SonidoFragment extends Fragment {

    MediaPlayer mp;
    Button play;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_sonido, container, false);
        mp = MediaPlayer.create(getActivity(), R.raw.perro);

        play =  root.findViewById(R.id.btplay);
        play.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mp.start();
            }
        });

        return root;
    }
}