package com.example.app_nav.ui.apk;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.app_nav.R;

import java.util.List;

public class ApkFragment extends Fragment {


    Button btyoutube;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_apk, container, false);


        btyoutube =  root.findViewById(R.id.btyoutube);
        btyoutube.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    Intent intent = new Intent(
                            Intent.ACTION_VIEW ,
                            Uri.parse("https://www.youtube.com/channel/UCRmoG8dTnv0B7y9uoocikLw"));
                    intent.setComponent(new ComponentName("com.google.android.youtube","com.google.android.youtube.PlayerActivity"));

                    PackageManager manager = getContext().getPackageManager();
                    List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
                    if (infos.size() > 0) {
                        getContext().startActivity(intent);
                    }else{
                        Toast.makeText(getContext(),"No tiene instalado youtube",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    System.out.println("Error: "+ e);
                }
            }
        });


        return root;
    }
}